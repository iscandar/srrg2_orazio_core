## SRRG2 Orazio Firmware
 Brand new firmware for the Orazio robot. Designed with modularity in mind,
 the firmware can be easily ported to different architectures without changing
 the upper layers.

In order to change architecture, the only thing that must be implemented are
the low level peripherals (like uart, digitalIO, encoders and so on) that are
contained in the ```arch``` folder.

Currently available architectures:
- ATMEGA 2560 (aka Arduino Mega)
- ATMEGA 328p  (aka Arduino Uno)
	with atmega328p sonar doesnt work, the memory of UNO its to small.
- DSPIC33FMC802F (on the MuIn DSNav board)

Partially developed
- ARM Cortex M0 (on the Teensy 3.6 board)

<<<<<<< HEAD
###Notes Atmega

=======
#### Notes Atmega
>>>>>>> b5312dab1044072720ffd198cc81a2b0bdd5bcea
- When you compile the firmware make attention to select the tipe of hbridg.
you can find it on make file.

#### Notes on DSPIC implementation

- This architecture only works with one of the possible H-Bridge implementations
available, the one with **pwm+direction**
(that corresponds to the ```mode 0``` in the firmware).
- The pin configuration is the following:
```
encoders: RP10 -> QEA1; RP11 -> QEB1; RP06 -> QEA2; RP05 -> QEB2;
pwm:      RP14 -> PWM1H1; RP15 -> PWM1DIR; RP12 -> PWM1H2; RP13 -> PWM2DIR;
uart:     RP7 -> RX; RP8 -> TX;
```
