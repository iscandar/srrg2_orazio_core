#pragma once
#include <stdint.h>
#include "orazio_packets.h"

#ifdef __cplusplus
extern "C" {
#endif

struct HBridgeOps;

typedef struct HBridge {
	struct HBridgeOps* ops;
	union {
				struct {
			uint8_t pwm_forward_pin;
			uint8_t pwm_backward_pin;
		} __attribute__((packed)) dualpwm;
			} __attribute__((packed)) params;
} __attribute__((packed)) HBridge;
		int8_t HBridge_setSpeed(struct HBridge* bridge, int16_t speed);
	void HBridgeDualPWM_init(HBridge* bridge,
                         uint8_t pwm_forward_pin,
                         uint8_t pwm_backward_pin);
	#ifdef __cplusplus
}
#endif
