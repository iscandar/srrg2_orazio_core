#include "orazio_hbridge.h"
#include "orazio_joints_internal.h"
#include "orazio_globals.h"
#include "orazio_hbridge_internal_HalfPWM.h"

static HBridge bridges[NUM_MOTORS] = {
  {
    .ops = 0
  },
  {
    .ops = 0
  }
};

void Orazio_hBridgeInit() {
  HBridgeHalfPWM_init(bridges, 2, 3);
  HBridgeHalfPWM_init(bridges + 1, 4, 5);
}

void Orazio_hBridgeApplyControl() {
  for (int i = 0; i < NUM_MOTORS; ++i) {
    HBridge* bridge = bridges + i;
    JointController* controller = joint_controllers + i;
    HBridge_setSpeed(bridge, controller->output);
  }
}
