#include "orazio_hbridge_internal_DualPWM.h"
#include "digio.h"
#include "pwm.h"
#include "orazio_packets.h"

typedef void (*HBridgeFiniFn)(HBridge* bridge);
typedef void (*HBridgeSetSpeedFn)(HBridge* bridge, int16_t speed);

typedef struct HBridgeOps {
  HBridgeFiniFn fini_fn;
  HBridgeSetSpeedFn setSpeed_fn;
} __attribute__((packed))  HBridgeOps;

void HBridgeDualPWM_fini(HBridge* bridge);
void HBridgeDualPWM_setSpeed(HBridge* bridge, int16_t speed);

static HBridgeOps h_bridge_ops[] =
{
  {
    .fini_fn = HBridgeDualPWM_fini,
    .setSpeed_fn = HBridgeDualPWM_setSpeed
  }
};

void HBridgeDualPWM_init(HBridge* bridge,
                         uint8_t pwm_forward_pin,
                         uint8_t pwm_backward_pin) {
  if (bridge->ops)
    (*bridge->ops->fini_fn)(bridge);

  bridge->ops = h_bridge_ops;
  bridge->params.dualpwm.pwm_forward_pin = pwm_forward_pin;
  bridge->params.dualpwm.pwm_backward_pin = pwm_backward_pin;
  PWM_setDutyCycle(pwm_forward_pin, 0);
  PWM_setDutyCycle(pwm_backward_pin, 0);
  PWM_enable(pwm_forward_pin, 1);
  PWM_enable(pwm_backward_pin, 1);
}

void HBridgeDualPWM_fini(HBridge* bridge) {
  PWM_enable(bridge->params.dualpwm.pwm_forward_pin, 0);
  PWM_enable(bridge->params.dualpwm.pwm_backward_pin, 0);
}

void HBridgeDualPWM_setSpeed(HBridge* bridge, int16_t speed) {
  const uint8_t fpwm_pin = bridge->params.dualpwm.pwm_forward_pin;
  const uint8_t bpwm_pin = bridge->params.dualpwm.pwm_backward_pin;
  if (speed > 255)
    speed = 255;
  if (speed < -255)
    speed = -255;
  if (speed >= 0) {
    PWM_setDutyCycle(fpwm_pin, speed);
    PWM_setDutyCycle(bpwm_pin, 0);
  } else {
    PWM_setDutyCycle(fpwm_pin, 0);
    PWM_setDutyCycle(bpwm_pin, -speed);
  }
}

int8_t HBridge_setSpeed(HBridge* bridge, int16_t speed) {
  (*bridge->ops->setSpeed_fn)(bridge, speed);
  return 0;
}
