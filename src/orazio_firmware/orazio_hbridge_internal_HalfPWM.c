#include "orazio_hbridge_internal_HalfPWM.h"
#include "digio.h"
#include "pwm.h"
#include "orazio_packets.h"

typedef void (*HBridgeFiniFn)(HBridge* bridge);
typedef void (*HBridgeSetSpeedFn)(HBridge* bridge, int16_t speed);

typedef struct HBridgeOps {
  HBridgeFiniFn fini_fn;
  HBridgeSetSpeedFn setSpeed_fn;
} __attribute__((packed))  HBridgeOps;


void HBridgeHalfPWM_fini(HBridge* bridge);
void HBridgeHalfPWM_setSpeed(HBridge* bridge, int16_t speed);

static HBridgeOps h_bridge_ops[] =
{
  {
    .fini_fn = HBridgeHalfPWM_fini,
    .setSpeed_fn = HBridgeHalfPWM_setSpeed
  }
};

void HBridgeHalfPWM_init(HBridge* bridge,
                         uint8_t pwm_pin,
                         uint8_t enable_pin) {
  if (bridge->ops)
    (*bridge->ops->fini_fn)(bridge);

  bridge->ops = h_bridge_ops;
  bridge->params.halfpwm.pwm_pin = pwm_pin;
  bridge->params.halfpwm.enable_pin = enable_pin;
  PWM_setDutyCycle(pwm_pin, 0);
  DigIO_setValue(enable_pin, 1);
  PWM_enable(pwm_pin, 1);
  DigIO_setDirection(enable_pin, 1);
}

void HBridgeHalfPWM_fini(HBridge* bridge) {
  uint8_t pwm_pin = bridge->params.halfpwm.pwm_pin;
  uint8_t enable_pin = bridge->params.halfpwm.enable_pin;
  PWM_setDutyCycle(pwm_pin, 0);
  DigIO_setValue(enable_pin, 0);
  PWM_enable(pwm_pin, 0);
  DigIO_setDirection(enable_pin, 0);
}

void HBridgeHalfPWM_setSpeed(HBridge* bridge, int16_t speed) {
  uint8_t pwm_pin = bridge->params.halfpwm.pwm_pin;
  if (speed > 255)
    speed = 255;
  if (speed < -255)
    speed = -255;
  speed = 127 + speed / 2;
  PWM_setDutyCycle(pwm_pin, speed);
}

int8_t HBridge_setSpeed(HBridge* bridge, int16_t speed) {
  (*bridge->ops->setSpeed_fn)(bridge, speed);
  return 0;
}
