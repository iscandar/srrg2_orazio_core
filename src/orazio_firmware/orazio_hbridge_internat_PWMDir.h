#pragma once
#include <stdint.h>
#include "orazio_packets.h"

#ifdef __cplusplus
extern "C" {
#endif

struct HBridgeOps;

typedef struct HBridge {
	struct HBridgeOps* ops;
	union {
				struct {
			uint8_t pwm_pin;
			uint8_t dir_pin;
			uint8_t brake_pin;
		} __attribute__((packed)) pwmdir;
			} __attribute__((packed)) params;
} __attribute__((packed)) HBridge;
	int8_t HBridge_setSpeed(struct HBridge* bridge, int16_t speed);
	void HBridgePWMDir_init(HBridge* bridge,
                        uint8_t pwm_pin,
                        uint8_t dir_pin,
                        uint8_t brake_pin);
	#ifdef __cplusplus
}
#endif
