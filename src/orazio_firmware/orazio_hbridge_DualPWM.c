#include "orazio_hbridge.h"
#include "orazio_joints_internal.h"
#include "orazio_globals.h"
#include "orazio_hbridge_internal_DualPWM.h"

static HBridge bridges[NUM_MOTORS] = {
  {
    .ops = 0
  },
  {
    .ops = 0
  }
};

void Orazio_hBridgeInit() {
  HBridgeDualPWM_init(bridges, 3, 2);
  HBridgeDualPWM_init(bridges + 1, 5, 4);
}
void Orazio_hBridgeApplyControl() {
  for (int i = 0; i < NUM_MOTORS; ++i) {
    HBridge* bridge = bridges + i;
    JointController* controller = joint_controllers + i;
    HBridge_setSpeed(bridge, controller->output);
  }
}
