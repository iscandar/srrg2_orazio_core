#include "orazio_hbridge.h"
#include "orazio_joints_internal.h"
#include "orazio_globals.h"
#include "orazio_hbridge_internat_PWMDir.h"

static HBridge bridges[NUM_MOTORS] = {
  {
    .ops = 0
  },
  {
    .ops = 0
  }
};

void Orazio_hBridgeInit() {
  HBridgePWMDir_init(bridges, 3, 12, 9);
  HBridgePWMDir_init(bridges + 1, 11, 13, 8);
}

void Orazio_hBridgeApplyControl() {
  for (int i = 0; i < NUM_MOTORS; ++i) {
    HBridge* bridge = bridges + i;
    JointController* controller = joint_controllers + i;
    HBridge_setSpeed(bridge, controller->output);
  }
}
