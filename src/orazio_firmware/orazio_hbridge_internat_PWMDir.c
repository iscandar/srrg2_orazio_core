#include "orazio_hbridge_internal_PWMDir.h"
#include "digio.h"
#include "pwm.h"
#include "orazio_packets.h"

typedef void (*HBridgeFiniFn)(HBridge* bridge);
typedef void (*HBridgeSetSpeedFn)(HBridge* bridge, int16_t speed);

typedef struct HBridgeOps {
  HBridgeFiniFn fini_fn;
  HBridgeSetSpeedFn setSpeed_fn;
} __attribute__((packed))  HBridgeOps;



void HBridgePWMDir_fini(HBridge* bridge);
void HBridgePWMDir_setSpeed(HBridge* bridge, int16_t speed);

static HBridgeOps h_bridge_ops[] =
{
  {
    .fini_fn = HBridgePWMDir_fini,
    .setSpeed_fn = HBridgePWMDir_setSpeed
  }
};

/* PWM+Dir Mode */
void HBridgePWMDir_init(HBridge* bridge,
                        uint8_t pwm_pin,
                        uint8_t dir_pin,
                        uint8_t brake_pin) {
  if (bridge->ops)
    (*bridge->ops->fini_fn)(bridge);

  bridge->ops = h_bridge_ops;
  bridge->params.pwmdir.pwm_pin = pwm_pin;
  bridge->params.pwmdir.dir_pin = dir_pin;
  bridge->params.pwmdir.brake_pin = brake_pin;

  DigIO_setDirection(dir_pin, Output);
  DigIO_setValue(dir_pin, 0);
  DigIO_setDirection(brake_pin, Output);
  DigIO_setValue(brake_pin, 0);
  PWM_enable(pwm_pin, 1);
  PWM_setDutyCycle(pwm_pin, 0);
}

void HBridgePWMDir_fini(HBridge* bridge) {
  PWM_enable(bridge->params.pwmdir.pwm_pin, 0);
  PWM_setDutyCycle(bridge->params.pwmdir.pwm_pin, 0);
  DigIO_setDirection(bridge->params.pwmdir.dir_pin, Input);
  DigIO_setValue(bridge->params.pwmdir.dir_pin, 0);
}

void HBridgePWMDir_setSpeed(HBridge* bridge, int16_t speed) {
  const uint8_t dir_pin = bridge->params.pwmdir.dir_pin;
  const uint8_t pwm_pin = bridge->params.pwmdir.pwm_pin;
  uint16_t pwm = 0;
  uint8_t dir = 0;
  if (speed >= 0) {
    pwm = speed;
    dir = 0;
  } else {
    pwm = -speed;
    dir = 1;
  }
  if (pwm > 255)
    pwm = 255;
  DigIO_setValue(dir_pin, dir);
  PWM_setDutyCycle(pwm_pin, pwm);
}

int8_t HBridge_setSpeed(HBridge* bridge, int16_t speed) {
  (*bridge->ops->setSpeed_fn)(bridge, speed);
  return 0;
}
