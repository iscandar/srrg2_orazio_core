#include "pins.h"
#include "digio.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
/*
it would be possible to save this vector inside the flash memory,
the problem is that the cost of the operation in order to modify the values
inside the vector are too high compared to the gain of free memory
*/
const Pin pins[] =
{
//0 digital pin 0 arduino 328p
  /*
  {
    .in_register=&PIND,
    .out_register=&PORTD,
   .dir_register=&DDRD,
   .bit=0,
   .tcc_register=0,
   .oc_register=0,
   .com_mask=0
      },
      //1 digital pin 1 arduino 328p
      {
   .in_register=&PIND,
   .out_register=&PORTD,
   .dir_register=&DDRD,
   .bit=1,
   .tcc_register=0,
   .oc_register=0,
   .com_mask=0
  },*/
//0 digital pin 7 arduino 328p
  {
    .in_register = &PIND,
    .out_register = &PORTD,
    .dir_register = &DDRD,
    .bit = 7,
    .tcc_register = 0,
    .oc_register = 0,
    .com_mask = 0
  },
//1 digital pin 4 arduino 328p
  {
    .in_register = &PIND,
    .out_register = &PORTD,
    .dir_register = &DDRD,
    .bit = 4,
    .tcc_register = 0,
    .oc_register = 0,
    .com_mask = 0
  },
//2 digital PWM pin 5 arduino 328p
  {
    .in_register = &PIND,
    .out_register = &PORTD,
    .dir_register = &DDRD,
    .bit = 5,
    .tcc_register = &TCCR0A,
    .oc_register = &OCR0B,
    .com_mask = (1 << COM0B1) | (1 << COM0B0)
  },
//3 digital PWM pin 6 arduino 328p
  {
    .in_register = &PIND,
    .out_register = &PORTD,
    .dir_register = &DDRD,
    .bit = 6,
    .tcc_register = &TCCR0A,
    .oc_register = &OCR0A,
    .com_mask = (1 << COM0A1) | (1 << COM0A0)
  },
//4 digital PWM pin 3 arduino 328p
  {
    .in_register = &PIND,
    .out_register = &PORTD,
    .dir_register = &DDRD,
    .bit = 3,
    .tcc_register = &TCCR2A,
    .oc_register = &OCR2B,
    .com_mask = (1 << COM2B1) | (1 << COM2B0)
  },
//5 digital PWM pin 11 arduino 328p
  {
    .in_register = &PINB,
    .out_register = &PORTB,
    .dir_register = &DDRB,
    .bit = 3,
    .tcc_register = &TCCR2A,
    .oc_register = &OCR2A,
    .com_mask = (1 << COM2A1) | (1 << COM2A0)
  },
//6 digital pin 8 arduino 328p
  {
    .in_register = &PINB,
    .out_register = &PORTB,
    .dir_register = &DDRB,
    .bit = 0,
    .tcc_register = 0,
    .oc_register = 0,
    .com_mask = 0
  },
//7 digital  pin 12 arduino 328p
  {
    .in_register = &PINB,
    .out_register = &PORTB,
    .dir_register = &DDRB,
    .bit = 4,
    .tcc_register = 0,
    .oc_register = 0,
    .com_mask = 0
  }
};
