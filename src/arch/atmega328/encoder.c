#include "encoder.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#define read_flash(array, pos) (pgm_read_byte(&(array[pos])))

#define NUM_ENCODERS 2
/*this masks as use for a reason, if you wanna change, make attention!
is useful remember that port_value after read, 
became 0x03 if i have 11 or 0x02 for 10 and 0x01 for 01
i use this mask to use the pins that have the timer in common to compare*/

#define ENCODER_MASK_ONE 0x11 // bit of PORT B
#define ENCODER_MASK_TWO 0x90 // bit of PORT D

typedef struct {
  uint16_t current_value;
  uint16_t sampled_value;
  uint8_t  pin_state;
} __attribute__((packed))  Encoder;

Encoder _encoders[NUM_ENCODERS] = {
  {0, 0},
  {0, 0}
};

static const int8_t _transition_table [] PROGMEM=
{
  0,  //0000
  -1, //0001
  1,  //0010
  0,  //0011
  1,  //0100
  0,  //0101
  0,  //0110
  -1, //0111
  -1, //1000
  0,  //1001
  0,  //1010
  1,  //1011
  0,  //1100
  1,  //1101
  -1,  //1110
  0   //1111
};

// initializes the encoder subsystem
void Encoder_init(void) {
  cli();
  PCICR |= (1 << PCIE0) | (1 << PCIE2);   // set interrupt on change, looking up PCMSK0 and PCMSK2

  //see the ENCODER_MASK_ONE
  DDRB &= ~(ENCODER_MASK_ONE);    //set encoder pins as input its the same as encoder mask
  PORTB |= (ENCODER_MASK_ONE);    //enable pull up resistors
  PCMSK0 |= (ENCODER_MASK_ONE);     // set PCINT0 to trigger an interrupt on state change

  //see the ENCODER_MASK_TWO
  DDRD &= ~(ENCODER_MASK_TWO);    //set encoder pins as input its the same as encoder mask
  PORTD |= (ENCODER_MASK_TWO);    //enable pull up resistors
  PCMSK2 |= (ENCODER_MASK_TWO);     // set PCINT0 to trigger an interrupt on state change
  sei();                        // turn on interrupts
}

// samples the encoders, saving the respective values in a temporary storage
void Encoder_sample(void) {
  cli();
  for (uint8_t i = 0; i < NUM_ENCODERS; ++i)
    _encoders[i].sampled_value = _encoders[i].current_value;
  sei();
}

// returns the number of the encoder
uint8_t Encoder_numEncoders(void) {
  return NUM_ENCODERS;
}

// returns the value of an encoder, when sampled with the Encoder_sample();
uint16_t Encoder_getValue(uint8_t num_encoder) {
  return _encoders[num_encoder].sampled_value;
}


//encoder 0 (associated to port B)
ISR(PCINT0_vect) {
  cli();
char port_value = PINB&ENCODER_MASK_ONE;

  if (port_value == ENCODER_MASK_ONE) {
    port_value = 0x03;
  } else if (port_value == 0x10) {
    port_value = 0x02;
  } else if (port_value == 0x01) { //redundant but useful for understanding
    port_value = 0x01;
  }
  uint8_t idx = (_encoders[0].pin_state << 2) | port_value ;
  _encoders[0].current_value += read_flash(_transition_table,idx);
  _encoders[0].pin_state = port_value;
  sei();
}

//encoder 1 (associated to port D)
ISR(PCINT2_vect) {
  cli();
 char port_value = PIND&ENCODER_MASK_TWO;
  if (port_value == ENCODER_MASK_TWO) {
    port_value = 0x03;
  } else if (port_value == 0x80) {
    port_value = 0x02;
  } else if (port_value == 0x10) {
    port_value = 0x01;
  }
  uint8_t idx = (_encoders[1].pin_state << 2) | port_value ;
  _encoders[1].current_value += read_flash(_transition_table,idx);
  _encoders[1].pin_state = port_value;
  sei();
}


