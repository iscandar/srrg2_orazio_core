#include "adc.h"
// adc not working, arduino uno with orazio havent enuf memory
void initAdc() {

	ADCSRA |= (1 << ADSC);
	ADCSRA |= (1 << ADEN);
	ADCSRA |= (1 << ADPS2 ) | (1 << ADPS1 ) | (1 << ADPS0 ); // faThese bits determine the division factor between the system clock frequency and the input clock to the adc 128
	ADMUX |= (1 << REFS0);
	ADMUX &= ~(1 << REFS1);
}
void closeAdc() {
	ADCSRA &= ~(1 << ADEN);
}

uint16_t readAdc(uint8_t channel) {
	uint16_t val;
	ADMUX = (1 << pinsAnalogic[channel].com_mask) | channel;
	ADCSRA |= (1 << ADSC); // start conversion
	while ( ADCSRA & ( 1 << ADSC ) );
	val = ADCH;
	ADCH = 0;
	ADCL = 0;
	ADCSRA |= (1 << ADIF);
	return val;
}
